const xhr = new XMLHttpRequest();
let commentsData = new Array();
const ul = document.querySelector('.comments ul'),
    btns = document.querySelector('.btn_wrap'),
    modalLoader = document.querySelector('.loader'),
    prev = document.querySelector('.btn_prev'),
    next = document.querySelector('.btn_next');
let currentPage = 1;
let commentsNum = 10;

xhr.open('GET', 'https://jsonplaceholder.typicode.com/comments');
xhr.send();

xhr.addEventListener('readystatechange', () => {
    modalLoader.classList.add('active_loader');

    if (xhr.readyState === 4 && xhr.status >= 200 && xhr.status < 300) {
        commentsData = JSON.parse(xhr.responseText);
        displayPaginationBtn(commentsData, commentsNum);
        showComments(commentsData, currentPage, commentsNum);
        btns.addEventListener('click', toggleBtn);
        modalLoader.classList.remove('active_loader');
    } else if (xhr.readyState === 4) {
        throw new Error('Помилка у запиті');
    }
})

const showComments = (arrData, page, numbers) => {
    if (!Array.isArray(arrData)) {
        throw new Error('З сервера прийшов не масив');
    }
    ul.innerHTML = '';
    page--;

    const start = page * numbers;
    const end = start + numbers;
    arrData.slice(start, end).forEach(obj => {
        ul.insertAdjacentHTML('beforeend',
            `<li class="comment">
                    <div class="comment_info">
                        <span class="comment_id">${obj.id}</span>
                        <span class="comment_name">${obj.name}</span>
                        <a href="mailto:${obj.email}" class="comment_email">${obj.email}</a>
                    </div>
                    <div class="comment_text">${obj.body}</div>
                </li>`)
    })
}


const displayPaginationBtn = (arrData, numbers) => {
    const pagesCount = Math.ceil(arrData.length / numbers);

    for (let i = 0; i < pagesCount; i++) {
        const input = document.createElement('input');
        input.type = 'button';
        input.value = i + 1;
        if (input.value == currentPage) {
            input.classList.add('active');
        }
        prev.before(input);
    }
}

const toggleBtn = e => {
    if (e.target.tagName !== 'INPUT') {
        return;
    } else {
        currentPage = e.target.value;
        const [...btn] = btns.children;
        btn.forEach(el => el.classList.remove('active'));
        e.target.classList.add('active');
    }
    checkDisableBtn();
    showComments(commentsData, currentPage, commentsNum);
}

prev.addEventListener('click', e => {
    if (currentPage > 1) {
        document.querySelector('.active').classList.remove('active');
        document.querySelector(`input[value="${currentPage}"]`).previousElementSibling.classList.add('active')
        currentPage--
    }
    checkDisableBtn();
    showComments(commentsData, currentPage, commentsNum);
})

next.addEventListener('click', e => {
    if (currentPage < commentsData.length / commentsNum) {
        document.querySelector('.active').classList.remove('active');
        document.querySelector(`input[value="${currentPage}"]`).nextElementSibling.classList.add('active')
        currentPage++
    }
    checkDisableBtn();
    showComments(commentsData, currentPage, commentsNum);
})

const checkDisableBtn = () => {
    if (currentPage == 1) {
        prev.disabled = true;
    } else {
        prev.disabled = false;
    }
    if (currentPage == commentsData.length / commentsNum) {
        next.disabled = true;
    } else {
        next.disabled = false;
    }
}